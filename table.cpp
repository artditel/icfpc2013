#include "table.h"
#include <cstdlib>
#include <set>

std::vector<Bits> generateCheckArgs() {
    std::set<Bits> set;
    for (Bits i = 0; i < 64; ++i) {
        set.insert(Bits(1) << i);
        set.insert(~(Bits(1) << i));
        set.insert(i);
    }
    while (set.size() < 256) {
        Bits r = rand() * Bits(1e9+7) + rand();
        set.insert(r);
    }
    return std::vector<Bits>(set.begin(), set.end());


}

std::vector<Bits> HashTable::CHECK_ARGS = generateCheckArgs();

