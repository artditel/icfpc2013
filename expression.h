#pragma once
#include <cstdint>
#include <boost/format.hpp>
#include <string>
#include <stdexcept>
#include <array>

#include "tribool.h"


class Expression;
typedef const Expression* CExpression;

class Expression {
public:
    explicit Expression() :
        op_(Op::Var)
    {}

    explicit Expression(Bits c)
    {
        if (c == 1) {
            op_ = Op::One;
        } else {
            op_ = Op::Zero;
        }
    }

    explicit Expression(Op op, CExpression a0) :
        op_(op)
    {
        args_[0] = a0;
        args_[1] = nullptr;
        args_[2] = nullptr;
    }

    explicit Expression(Op op, CExpression a0, CExpression a1) :
        op_(op)
    {
        args_[0] = a0;
        args_[1] = a1;
        args_[2] = nullptr;
    }

    explicit Expression(Op op, CExpression a0, CExpression a1, CExpression a2) :
        op_(op)
    {
        args_[0] = a0;
        args_[1] = a1;
        args_[2] = a2;
    }

    const Expression& arg(size_t n) const {
        return *args_[n];
    }

    Bits operator()(Bits x) const {
        switch (op_) {
            case Op::If0:
                return !arg(0)(x) ? arg(1)(x) : arg(2)(x);

            case Op::Not:
                return ~arg(0)(x);

            case Op::Shl1:
                return arg(0)(x) << 1;

            case Op::Shr1:
                return arg(0)(x) >> 1;

            case Op::Shr4:
                return arg(0)(x) >> 4;
            case Op::Shr16:
                return arg(0)(x) >> 16;

            case Op::And:
                return arg(0)(x) & arg(1)(x);

            case Op::Or:
                return arg(0)(x) | arg(1)(x);

            case Op::Xor:
                return arg(0)(x) ^ arg(1)(x);

            case Op::Plus:
                return arg(0)(x) + arg(1)(x);

            case Op::Zero:
                return 0;
            case Op::One:
                return 1;

            case Op::Var:
                return x;
        }
    }

    std::string str() const {
        switch (op_) {
            case Op::If0:
                return boost::str(boost::format("(if0 %s %s %s)") %
                        arg(0).str() % arg(1).str() % arg(2).str());

            case Op::Not:
                return boost::str(boost::format("(not %s )") % arg(0).str());

            case Op::Shl1:
                return boost::str(boost::format("(shl1 %s )") % arg(0).str());

            case Op::Shr1:
                return boost::str(boost::format("(shr1 %s )") % arg(0).str());

            case Op::Shr4:
                return boost::str(boost::format("(shr4 %s )") % arg(0).str());

            case Op::Shr16:
                return boost::str(boost::format("(shr16 %s )") % arg(0).str());

            case Op::And:
                return boost::str(boost::format("(and %s %s)") %
                        std::min(arg(0).str(), arg(1).str()) % std::max(arg(0).str(), arg(1).str()));

            case Op::Or:
                return boost::str(boost::format("(or %s %s)") %
                        std::min(arg(0).str(), arg(1).str()) % std::max(arg(0).str(), arg(1).str()));

            case Op::Xor:
                return boost::str(boost::format("(xor %s %s)") %
                        std::min(arg(0).str(), arg(1).str()) % std::max(arg(0).str(), arg(1).str()));

            case Op::Plus:
                return boost::str(boost::format("(plus %s %s)") %
                        std::min(arg(0).str(), arg(1).str()) % std::max(arg(0).str(), arg(1).str()));

            case Op::Zero:
                return "0";
            case Op::One:
                return "1";

            case Op::Var:
                return "x";
        }
    }

    const Op op() const {
        return op_;
    }


    bool isZero() const {
        return tribool() == Tribool(0);
    }

    bool isConst() const {
        return tribool().isConst();

    }

    bool getConst() const {
        return tribool().value();
    }

    Tribool tribool() const {
        switch (op_) {
            case Op::If0:
                return arg(1).tribool() * arg(2).tribool();

            case Op::Not:
                return ~arg(0).tribool();

            case Op::Shl1:
                return arg(0).tribool() << 1;

            case Op::Shr1:
                return arg(0).tribool() >> 1;

            case Op::Shr4:
                return arg(0).tribool() >> 4;
            case Op::Shr16:
                return arg(0).tribool() >> 16;

            case Op::And:
                return arg(0).tribool() & arg(1).tribool();

            case Op::Or:
                return arg(0).tribool() | arg(1).tribool();

            case Op::Xor:
                return arg(0).tribool() ^ arg(1).tribool();

            case Op::Plus:
                return arg(0).tribool() + arg(1).tribool();

            case Op::Zero:
                return Tribool(Bits(0));
            case Op::One:
                return Tribool(Bits(1));

            case Op::Var:
                return Tribool();
        }
    }


    bool operator == (const Expression& expr) const {
        if (op_ != expr.op_) {
            return false;
        }
        return args_ == expr.args_;
    }

    static const Expression* allocate(Expression e) {
        static const size_t MAX = 600000000L;
        static std::vector<Expression> data(MAX);
        static size_t size = 0;
        if (size == MAX) {
            throw std::out_of_range("No more memory for expressions");
        }
        data[size] = e;
        return &data[size++];
    }
private:

    Op op_;
    std::array<const Expression*, 3> args_;
};

Bits eval(CExpression e, Bits input);
std::string toString(CExpression e);
