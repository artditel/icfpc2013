#pragma once
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <boost/functional/hash.hpp>

#include "expression.h"

class HashTable {
public:
    typedef size_t KeyType;

    HashTable() {
        insert(1, Expression::allocate(Expression(0)));
        insert(1, Expression::allocate(Expression(1)));
        insert(1, Expression::allocate(Expression()));
        inserts_ = 0;
    }

    void insert(int n, CExpression e) {
        if (e->isConst()) {
            Bits constant = e->getConst();
            if(!consts_.insert(constant).second) {
                return;
            }
        }
        inserts_ ++;
        auto hash_code = hash(e);
        if (map_.count(hash_code))
            return;
        vector_[n].push_back(e);
        map_[hash_code] = e;
    }

    size_t inserts() const {
        return inserts_;
    }

    std::vector<CExpression> get(const std::vector<Bits>& values) const {
        size_t seed = 0;
        for (Bits value: values) {
            boost::hash_combine(seed, value);
        }
        auto it = map_.find(seed);
        if (it != map_.end()) {
            return {it->second};
        } else {
            return {};
        }
    }


    const std::vector<CExpression>& getVector(int n) const {
        return vector_[n];
    }

    size_t size() const {
        return map_.size();
    }


    static std::vector<Bits> CHECK_ARGS;

private:
    KeyType hash(CExpression e) const {
        size_t seed = 0;
        for (Bits arg: CHECK_ARGS) {
            boost::hash_combine(seed, eval(e, arg));
        }
        return seed;
    }

    std::unordered_map<KeyType, CExpression> map_;
    std::unordered_set<Bits> consts_;
//    std::unordered_map<KeyType, std::unordered_set<std::string>> sets_;
    std::vector<CExpression> vector_[31];
    size_t inserts_;
};
