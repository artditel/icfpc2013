#include "generator.h"

#include <algorithm>
#include <iostream>
#include <vector>
#include <cstdlib>


int main(int argc, char** argv) {
    if (argc != 2) {
        std::cerr << "Usage [basename] max_program_length" << std::endl;
        return 1;
    }
    size_t programLength = atoi(argv[1]);
    HashTable table;


    size_t inserts = 0;
    for (size_t i = 2; i <= programLength; ++i) {
        std::cerr << "Generating table for size: " << i << std::endl;
        generate(i, table);
        std::cerr << "Inserted: " << table.inserts() - inserts << " total: " << table.inserts() << std::endl;
        inserts = table.inserts();
    }
    for (size_t i = programLength + 1; i <= programLength + 2; ++i) {
        std::cerr << "Generating if0 for size: " << i << std::endl;
        generate0(i, table);
        std::cerr << "Inserted: " << table.inserts() - inserts << " total: " << table.inserts() << std::endl;
        inserts = table.inserts();
    }
    std::cout << "ready" << std::endl;


//    table.printDuplicates();

    while (true) {
        for (auto arg: HashTable::CHECK_ARGS) {
            std::cout << " " << arg;
        }
        std::cout << std::endl;
        std::vector<Bits> values(HashTable::CHECK_ARGS.size());
        Bits bit;
        for (auto& value: values) {
            std::cin >> value;
        }

        std::vector<CExpression> candidates = table.get(values);

        while (true) {
            if (candidates.empty()) {
                std::cerr << "No candidates left!" << std::endl;
                std::cout << "fail" << std::endl;
                break;
            }
            auto guess = candidates.front();
            std::cout << toString(guess) << std::endl;
            std::string status;
            std::cin >> status;
            if (status == "win") {
                break;
            } else if (status == "mismatch") {
                Bits arg, expected, got;
                std::cin >> arg >> expected >> got;
                auto pend = std::remove_if(
                        candidates.begin(),
                        candidates.end(),
                        [arg, expected] (CExpression e) {
                            return eval(e, arg) != expected;
                            });
                candidates.resize(pend - candidates.begin());
            } else {
                std::cerr << "Unknown status: " << status << std::endl;
                break;
            }
        }
    }
}
